@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div id="post-form">
                        <form method="post" action="{{ route('posts.store')}}" id="contactform">
                                @csrf
                                <div class="form-group">
                                    <input name="uname" id="uname" type="text" class="" placeholder="Your Name" />
                                </div>
                                <div class="form-group">
                                    <input name="email" id="email" type="email" class="" placeholder="E-mail address" />
                                </div>
                                <div class="form-group">
                                    <textarea name="msg" id="msg" class="" placeholder="Message *"></textarea>
                                </div>
                                <input type="submit" id="submit" class="btn btn-success" value="Send message">
                        </form>
                    </div>
                </div>
            </div>
            <div class="card">
                    <div class="card-header">Posts</div>
                @foreach($posts as $post)
                <div class="card">
                    <div class="card-header">{{ $post->uname }} | <span class="glyphicons glyphicon-email">&nbsp;</span><a href="emailto:{{ $post->email }}">{{ $post->email }}</a></div>
                    <div class="card-body">{{ $post->msg }}</div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection